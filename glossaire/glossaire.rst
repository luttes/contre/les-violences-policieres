.. index::
   ! Glossaire

.. _glossaire:

=====================
Glossaire
=====================

.. glossary::


   BAC
       Brigades Anti Criminalité

   BRAV-M
       Brigade de Répression de l’Action Violente Motorisée

   PMC
       Produit de Marquage Codé

       - https://reporterre.net/Sainte-Soline-les-autorites-pistent-les-manifestants-grace-a-un-produit-invisible

