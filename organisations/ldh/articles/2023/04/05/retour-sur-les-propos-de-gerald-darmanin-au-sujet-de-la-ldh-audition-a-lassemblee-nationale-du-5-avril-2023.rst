
.. index::
   pair: Communiqué LDH; 2023-04-05

.. _communique_LDH_2023_04_05:

==========================================================================================================================================================================================================
2023-03-25 Communiqué LDH **Retour sur les propos de Gérald Darmanin au sujet de l’observation de la LDH à Sainte-Soline (audition à l’Assemblée nationale du 5 avril 2023)**
==========================================================================================================================================================================================================

- https://www.ldh-france.org/retour-sur-les-propos-de-gerald-darmanin-au-sujet-de-la-ldh-audition-a-lassemblee-nationale-du-5-avril-2023/
- https://www.ldh-france.org/wp-content/uploads/2023/04/CP-LDH-Audition-G-Darmanin-05042023.pdf

Le ministre de l’Intérieur a mis en cause ce jour la participation de la
LDH (Ligue des droits de l’Homme) à des observatoires citoyens qui ont
notamment documenté le dispositif de maintien de l’ordre sur la zone de
Sainte-Soline dans le cadre des mobilisations contre les « mégabassines »,
les 24-26 mars 2023.

Il opère une confusion dommageable sur la notion d’observateur indépendant,
indépendance qui s’entend vis-à-vis des pouvoirs publics et non des positions
que peut prendre la LDH comme association défendant les droits et libertés indivisibles.

Les trois arguments qu’il mobilise pour ce faire sont, de plus, faux ou biaisés :


L’allégation que la LDH serait « un observateur qui a appelé à manifester malgré la manifestation interdite »
=================================================================================================================

En tant qu’association, la LDH n’a pas appelé à manifester à Sainte-Soline.

Au niveau local, de manière autonome, comme les statuts de la LDH le prévoient,
deux sections de la LDH ont soutenu les rassemblements prévus les 24-26 mars
avant que les interdictions de manifester n’aient été prises et le comité
régional Poitou-Charentes a appelé dans un second temps à la mobilisation,
sans appeler à manifester, en précisant qu’un stand LDH serait tenu à Melle,
lieu d’un rassemblement déclaré et qui n’a pas été interdit.

Au-delà, la LDH n’est pas uniquement une vigie des droits de l’Homme, elle
prend comme toute association mobilisée dans le champ citoyen des positions
qui peuvent déplaire aux pouvoirs publics.

Au regard du droit international qui protège le statut d’observateur, c’est
plutôt un gage d’indépendance vis-à-vis de ceux-ci.

**Le ministre de l’Intérieur s’affranchit donc sur ce point de la réalité des faits.**

Le fait que la LDH a « attaqué l’arrêté de la préfète qui empêchait le transport d’armes », ce qui ne serait « pas très pacifique »
=========================================================================================================================================

La LDH a en effet formé un recours de principe en référé-liberté contre
les arrêtés pris par la préfète des Deux-Sèvres et le préfet de la Vienne :ref:`[1] <1_2023_04_05>`
prévoyant l’interdiction « d’armes par destination ».

La LDH contestait la définition choisie, qui méconnaissait la jurisprudence
du Conseil constitutionnel refusant l’extension a priori de la notion
d’arme à tout objet pouvant être utilisé comme projectile.

Dans le cadre limité qui est le sien, le juge des référés n’a pas donné
droit à cette demande de la LDH, mais celle-ci saisit le tribunal administratif au fond.

**Les associations se portant devant les juridictions sont-elles dangereuses selon M. Darmanin ?**

L’assertion que « le tribunal administratif de Poitiers lui-même n’a pas donné le statut d’observateur » à la LDH
======================================================================================================================

Le tribunal administratif de Poitiers, saisi par la LDH en référé-liberté,
a reconnu que la préfète des Deux-Sèvres avait commis une illégalité :ref:`[2] <2_2023_04_05>`
en déniant par principe aux observateurs indépendants la protection
particulière dont ils doivent bénéficier lors des manifestations, comme
pour les journalistes :ref:`[3] <3_2023_04_05>`

Au demeurant, ce n’est pas l’Etat qui confère un statut d’observateur à
telle ou telle organisation.

La qualité d’observatrice et d’observateur est reconnue par le droit
international au regard de sa mission pendant la manifestation et non à
l’association en tant que telle.

Les autorités n’ont pas à donner ou ne pas donner le statut d’observateur.

Elles doivent simplement reconnaître et garantir leur droit à la protection
conféré par le droit international, en vertu notamment de l’article 21
du Pacte international relatif aux droits civils et politiques régulièrement
signé et ratifié par la France.

Le Conseil d’Etat l’a rappelé dans sa décision du 10 juin 2021 sur le
schéma national du maintien de l’ordre : ils doivent être considérés à
l’instar des journalistes.

De même que pour l’arrêté « armes », dans le cadre limité qui est le sien,
le juge des référés n’a pas donné droit à la demande de la LDH, mais
celle-ci saisit le tribunal administratif au fond.

**Ce que souligne la mauvaise foi du ministre, c’est sa détermination à
piétiner le droit international protégeant le statut d’observateur et,
avec lui, la liberté d’expression.**

Les observatrices et observateurs sont déjà inquiétés sur le terrain,
car les errements qu’ils dénoncent et rendent visibles constituent un
contre-pouvoir citoyen.

Quel est le but recherché aujourd’hui par le ministre, sinon d’empêcher
de documenter l’action des forces de l’ordre ?

Paris, le 5 avril 2023

.. _1_2023_04_05:

[1]
=====

Arrêtés du 17 mars portant interdiction temporaire du port et du transport
d’armes, toutes catégories confondues, de munitions et d’objets pouvant
constituer une arme par destination.

.. _2_2023_04_05:


[2]
====

(Cons.5). Comme a pu en juger le Conseil d’Etat, n°444849, Schéma national
du maintien de l’ordre (SNMO), 10 juin 2021.
Décision annulant les dispositions du SNMO permettant d’interpeller
et de poursuivre les observateurs.

.. _3_2023_04_05:


[3]
====

La préfète considérait que « les observateurs de la Ligue des droits
de l’Homme présents sur les lieux de manifestation seront assimilés
à des manifestants et devront se confirmer non seulement aux interdictions
administratives de manifester sous peine d’être verbalisés (…)
et se conformer aux ordres de dispersion en cas d’attroupement susceptible
de générer des troubles à l’ordre public », privant de facto, les
observateur.ices de la possibilité d’exercer leur mission d’observation.



Rappel : Le rôle des observatoires des pratiques policières et des libertés publiques
============================================================================================

Les observatoires ont pour objectif de documenter, de manière objective,
les pratiques de maintien de l’ordre, d’informer les personnes participant
à des rassemblements et des manifestations, d’alimenter le débat d’intérêt
public sur les pratiques de maintien de l’ordre, d’identifier les éventuelles
dérives et de rédiger des rapports publics à l’attention notamment des pouvoirs publics.

La qualité d’observateur.ice ne se donne pas, elle doit être constatée
et respectée.

L’indépendance des observateur.ices, telle que reconnue en droit, s’exerce
vis-à-vis de l’Etat et non au regard des associations qui composent les différents observatoires.

Autrement dit, les autorités n’ont pas à conférer la qualité d’observateur.ice
à une organisation ou à personne.

En revanche, elles doivent garantir la protection des observateur.ices,
telle qu’exigé par le droit international et régional.

Ainsi, le Comité des droits de l’Homme des Nations Unies rappelle que
les observateurs bénéficient de la protection offerte par l’article 21
du Pacte international relatif aux droits civils et politiques en ce
qu’ils « jouent un rôle particulièrement important pour ce qui est de
permettre la pleine jouissance du droit de réunion pacifique.

Ces personnes ont droit à la protection offerte par le Pacte.

Il ne peut pas leur être interdit d’exercer ces fonctions ni leur être
imposé de limites à l’exercice de ces fonctions, y compris en ce qui
concerne la surveillance des actions des forces de l’ordre.

Ils ne doivent pas risquer de faire l’objet de représailles ou d’autres
formes de harcèlement, et leur matériel ne doit pas être confisqué ou
endommagé.

Même si une réunion est déclarée illégale et est dispersée, il n’est
pas mis fin au droit de la surveiller.

La surveillance des réunions par les institutions nationales des droits
de l’homme et les organisations non gouvernementales constitue une bonne pratique. »

En outre, la Commission de Venise du Conseil de l’Europe et l’OSCE rappellent
que la présence des observateurs lors des rassemblements doit être
juridiquement garantie sans que les autorités des Etats puissent entraver
l’exercice de ce droit, que les manifestations soient couvertes soient
ou non pacifiques, et cela y compris en présence d’ordre de dispersion
à l’attention des participants aux rassemblements.

Enfin, le Conseil d’État, soit la plus haute juridiction administrative,
a rappelé dans une décision en date du 10 juin 2021 que les observateurs
indépendants doivent pouvoir « continuer d’exercer librement leur mission
lors de la dispersion d’un attroupement sans être tenus de quitter les lieux,
dès lors qu’ils se placent de telle sorte qu’ils ne puissent être confondus
avec les manifestants et ne fassent obstacle à l’action des forces de l’ordre. »

