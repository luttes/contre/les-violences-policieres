.. index::
   pair: Pétition; BRAV-M

.. _petitions_brave_m_2023_03_22:

===============================================================================
2023-03-22 ⚖️ **La brigade motorisée BRAV-M n’a pas sa place en démocratie !**
===============================================================================


Pétition
==========

- https://speakout.lemouvement.ong/campaigns/Stop-BRAV
- https://www.francetvinfo.fr/economie/retraite/reforme-des-retraites/info-franceinfo-reforme-des-retraites-trois-deputes-lfi-demandent-la-dissolution-la-brav-m-a-gerald-darmanin_5725115.html


**Ces derniers jours, nous assistons à des scènes insupportables, dignes
de régimes autoritaires**.

**Des étudiants alignés à genoux mains sur la tête, des manifestants gazés,
encerclés pendant des heures ou arrêtés arbitrairement**.

Parmi ces pratiques abusives, les charges policières sur la foule sont
particulièrement choquantes.

Sur certaines images, on voit des agents de la BRAV-M charger et frapper
à coups de matraques des citoyens pétrifiés, parfois blottis au sol,
qui ne présentaient aucune menace à l’ordre public.

**Cette brutalité policière n’a pas sa place en démocratie !**

**Nous demandons la dissolution de la BRAV-M sans délai.**

Peu connues jusqu'ici, les Brigades de répression des actions violentes
motorisées (BRAV-M) ont été créées en 2019 à Paris dans le cadre de la
mobilisation des Gilets jaunes.
Ces brigades sont souvent comparées aux PVM (pelotons de voltigeurs motorisés)
dissous en 1986 après la mort de Malik Oussekine, frappé mortellement
par plusieurs policiers.

L’histoire va-t-elle se répéter ? Attend-on qu’il y ait des morts avant de réagir ?

Nasses, arrestations arbitraires, aujourd’hui beaucoup de pratiques des
forces de l’ordre sont pointées du doigt par les manifestants et les
associations de défense des droits humains.

Parmi ces pratiques liberticides, la violence inouïe exercée par la
BRAV-M nous préoccupe au plus haut point.

Alors que de nouvelles mobilisations contre la réforme des retraites
sont prévues dans les prochains jours, c’est le moment d’interpeller
le Président de la République pour empêcher de nouvelles violences et
de potentiels blessés.

Nous ne voulons plus voir des citoyens se faire frapper dans la rue par la police.

**Nous ne voulons pas être en danger lorsque nous manifestons.**

**Nous ne voulons pas d’un pays dans lequel la BRAV-M existe**.


https://www.change.org/p/pour-une-dissolution-des-brav-m
==============================================================

- https://www.change.org/p/pour-une-dissolution-des-brav-m

Des jeunes hommes menacés, humiliés et agressés sexuellement, un coup de
poing en plein visage d’un autre..Des coups de matraques en pleine circulation..

Plusieurs témoignages et vidéos circulent pour attester d’un usage
disproportionné de la force et d’un abus de pouvoir exercé par ces unités
(mais aussi par les CRS) pendant les manifestations contre la réforme
des retraites.

La Défenseure des droits a d’ailleurs récemment rappelé certaines de ses
recommandations.

Pourquoi dissoudre les BRAV-M ?
------------------------------------------

Les BRAV-M (brigades de répression de l'action violente motorisées) sont
des unités créées en 2019 pendant le mouvement des gilets jaunes par
Didier Lallement, ex préfet de Paris, contre qui une plainte a été déposée
et instruite en raison de ses méthodes de maintien de l’ordre.

Des méthodes bien connues de  l'Observatoire Parisien des Libertés Publiques
qui a publié deux rapports  très détaillés à ce sujet (Paris - Bordeaux).

Des méthodes, soutenues par le Ministère de l’intérieur, qui privilégient
un comportement agressif vis-à-vis des manifestant‧es, au mépris de leurs
droits.

Des méthodes inadaptées sur le plan du maintien de l'ordre et parfaitement illégales.

Ce type d’unité avait pourtant été dissous en 1986, (les “Voltigeurs”),
après que Malik Oussekine, étudiant, ait été tué sous les coups de
plusieurs de leurs membres (ces unités avaient été créées en 1969
après les mobilisations de mai 68).

Pourquoi dissoudre les BRAV-M ?
--------------------------------------

Parce que plus de la moitié des Français (55%) estime que les policiers
font un usage excessif de la violence. (Ipsos/Sopra Steria);

Parce que même si parmi les 150 000 policiers, toutes et tous ne
commettent pas de délits et de crimes, nous devons dénoncer les faits
de violences policières lorsqu’ils sont commis par certains.

Ceux-là, doivent être jugés et non mutés ou récompensés comme Christophe
Castaner, ex ministre de l'intérieur, qui a reçu la légion d’honneur
en ce début d’année, avec une trentaine de personnes mutilées à son
tableau de chasse…un service rendu la nation…?

Parce que les dépressions, burn-out et suicides s'accumulent au sein
d'une majorité de policiers qui protège et conseille la population
quotidiennement, tandis que d'autres, violents et même pour certains
sexistes, racistes, se complaisent dans des pratiques d'abus de pouvoir
entraînant des dérives extrêmement inquiétantes ayant déjà causé la mort.

Ceux-là, il faut les dénoncer, les sanctionner et les bannir de la profession.

Parce que les victimes ont le droit d'obtenir justice.  Il faut démanteler les BRAV-M.


https://petitions.assemblee-nationale.fr/initiatives/i-1319
==============================================================

- https://petitions.assemblee-nationale.fr/initiatives/i-1319

La répression policière qui s’abat sur notre pays doit conduire à remettre
à l’ordre du jour l’impératif démantèlement de la BRAV-M.

Le pays étouffe de témoignages d’exactions violentes et brutales commises
par ces brigades motorisées à l’encontre des manifestants qui tentent
de faire entendre leur opposition à un projet de régression sociale.

Ces témoignages circulent dans le monde entier, interpellent la communauté
internationale et entachent l’image de notre pays. Ils font état d’un
emploi disproportionné et arbitraire de la force et en contrariété avec
le schéma national de maintien de l'ordre.

Brigade créée en mars 2019 sous l’impulsion du préfet Lallement pour
bâillonner le mouvement des Gilets Jaunes, elle est devenue l’un des
symboles de la violence policière.

Loin d’assurer un retour à l’apaisement, son action participe de
l’augmentation des tensions, y compris à l’encontre des forces de l’ordre.

Aussi, son mode d’intervention évoque le souvenir des voltigeurs, des
duos de policiers montés sur une moto pour disperser les manifestants,
brigade dissoute en 1986 après l’assassinat de Malik Oussekine, roué
de coups par trois voltigeurs en marge de contestations étudiantes.

Le droit de manifester convoque des droits fondamentaux garantis par
notre corpus constitutionnel : la libre communication des pensées et
des opinions, la liberté d’aller et venir ou encore la liberté de réunion
et d’association.

Il nous incombe de les préserver et de nous opposer à tout usage excessif
de la force qui viendrait les entraver.
Nous sommes du côté de la colère sociale des manifestants et non de
celui du flash-ball, des grenades, des matraques et des nasses.

Nous demandons la dissolution de la BRAV-M. Stoppons le massacre.


Affaire classée sans suite le mercredi 5 avril 2023
--------------------------------------------------------

- :ref:`petition_brav_m_classee_2023_04_05`



