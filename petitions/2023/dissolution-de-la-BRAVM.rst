.. index::
   pair: Dissolution; BRAV-M

.. _dissolution_brave_m_2023_04_10:

===============================================================================
2023-04-10 ⚖️ **La brigade motorisée BRAV-M n’a pas sa place en démocratie !**
===============================================================================

- https://www.change.org/u/1304095140
- https://www.change.org/p/dissolution-de-la-bravm-7d9733b3-df52-4491-8a2d-0eebb41a9670

Pétition
==========

- https://www.change.org/p/dissolution-de-la-bravm-7d9733b3-df52-4491-8a2d-0eebb41a9670
