.. index::
   pair: Pétition; Stop à l'escalade répressive


.. _petition_stop_escalade_2023_03_27:

================================================================================
2023-03-27 **Stop à l’escalade répressive** Pétition à l’initiative de la  LDH
================================================================================

- https://www.ldh-france.org/retraites-stop-a-lescalade-repressive/

Texte
========

Madame la Première ministre, Monsieur le ministre de l’Intérieur,

Le territoire français et les grandes villes plus particulièrement sont
depuis plusieurs jours le théâtre d’opérations de maintien de l’ordre
de grande ampleur, violentes et totalement disproportionnées.

Ces derniers jours ont vu le retour:

- des nasses illégales,
- de l’usage d’armes mutilantes comme le LBD et les grenades de désencerclement ou
  explosives,
- du gazage à outrance,
- de l’emploi de policiers non formés au maintien de l’ordre
  et réputés pour leur violence, en particulier la brigade de répression
  de l’action violente motorisée (BRAV-M) et les brigades anti criminalité (BAC),

avec des interpellations et des verbalisations indiscriminées, du matraquage
systématique et des violences gratuites et attentatoires à la dignité,
parfois même à l’intégrité physique des personnes.

Président de la LDH (Ligue des droits de l’Homme), association qui œuvre
depuis 1898 à la défense des droits et libertés, je ne peux que constater
que vous faites **le choix d’une escalade répressive pour briser des
mouvements sociaux légitimes**.

Votre politique brutale plonge aujourd’hui le pays dans une situation
particulièrement alarmante pour la démocratie.

Avec cette pétition, la LDH et l’ensemble des citoyennes et citoyens
signataires appellent votre gouvernement à la raison et exigent de vous,
Madame la Première ministre, Monsieur le ministre de l’Intérieur, le
respect des droits fondamentaux.


**Sous les plus brefs délais, nous demandons :**

- une révision des méthodes d’intervention de maintien de l’ordre
  (notamment la suppression de la :term:`BRAV-M` et de la nasse) ;
- l’interdiction des **techniques d’immobilisation mortelles et des armes de guerre** ;
- la suppression du délit de participation volontaire à un groupement
  formé en vue de commettre des violences ou des dégradations (article 222-14-2 du code pénal)
  et la fin des gardes à vue “préventives”, de la politique du chiffre
  sur les interpellations ;
- un traitement judiciaire équitable des faits de violences policières
  et un meilleur encadrement des procédures d’outrage et rébellion ;
- une réforme des conditions autorisant les contrôles d’identité, aujourd’hui
  détournés de leur objet à des fins de pression et de répression ;
- le respect de la qualité des observatrices et observateurs indépendants
  et des journalistes, et plus généralement de la liberté d’informer et
  de rendre compte des pratiques des forces de l’ordre.

C’est là le préalable à un avenir commun apaisé.

Car le risque d’un ordre qui déborde, c’est d’être lui-même bientôt débordé
face aux tensions et à la radicalité qu’il exacerbe.

Patrick Baudouin, président de la LDH

Signez la pétition !
=========================

- https://www.change.org/p/retraites-stop-%C3%A0-l-escalade-r%C3%A9pressive
