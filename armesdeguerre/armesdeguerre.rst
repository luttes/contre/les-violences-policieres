.. index::
   pair: Armes de guerre ; Fascisme

.. _armes_de_guerre:

=================================================================
Armes de guerre
=================================================================

.. toctree::
   :maxdepth: 5

   grenades/grenades
   lanceurs/lanceurs
