.. index::
   pair: Armes de guerre ; de Désencerclement D.B.D

.. _dbd_gmd_dmp:

=================================================================================================================================================
Grenades de **Dispersion (DBD), Désencerclement GMD), Protection (DMP)** classées comme armes de catégorie A2 et donc comme *matériel de guerre*
=================================================================================================================================================

- https://maintiendelordre.fr/grenades-de-desencerclement-d-b-d-sae-440/
- https://fr.wikipedia.org/wiki/Grenade_de_d%C3%A9sencerclement
- :ref:`glif4`



Description
=============

:Source: https://maintiendelordre.fr/grenades-de-desencerclement-d-b-d-sae-440/


Les Dispositifs Balistiques de Dispersion **(DBD)**, Dispositifs Manuels de
Protection **(DMP)** ou Grenades à main de désencerclement **(GMD)** sont
fabriqués par les sociétés Alsetex, SAPL et Verney Carron et équipent les
forces de l’ordre depuis une décision de Nicolas Sarkozy en 2004.

Le corps est composé de **18 projectiles en caoutchouc dur de 9 g**.

La grenade explose avec un niveau sonore de **160 Db** en projetant des
fragments d’une force cinétique de 80 joules dans un rayon efficace de 15 m
et jusqu’à 30 m.

Elle surpasse le bruit d’un avion au décollage et dépasse le seuil de
douleur sonore.
Au delà de **120 dB**, des bruits très brefs provoquent immédiatement des
dommages irréversibles.

Elle est amorcée par un bouchon allumeur d’un retard de 1.5 s. Il existe
également une version pouvant être chambrée dans un lanceur de 56 mm permettant
d’atteindre une distance de 60 m mais n’est pas utilisée en maintien de
l’ordre en France.

Elle est classée comme arme de catégorie A2 et donc comme *matériel de guerre*.

- 450 vitesse des galets en km/h
- 800 Grenades utilisées par les CRS le 1er décembre 2018 à Paris
- 40000 Grenades achetées le 1er mai 2019 pour 1,840,000 euros


Elles ne sont théoriquement utilisées que “dans le cadre d’autodéfense rapprochée
et non pour le contrôle d’une foule à distance” qui permet “dans le respect des
lois et des règlements, une réponse graduée et proportionnée à une situation
de danger lorsque l’emploi légitime de la force s’avère nécessaire”, avant
utilisation de l’arme individuelle.

Pour éviter les blessures graves à la tête elles doivent être lancées au ras
du sol uniquement, sauf dans le cas ou l’utilisation de l’arme individuelle
est légale.

