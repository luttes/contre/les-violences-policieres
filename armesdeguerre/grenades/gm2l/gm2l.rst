.. index::
   pair: Armes de guerre ; Grenade GM2 L

.. _gm2l:

======================================================================================================
Grenade instantanée **GM2 L** classée comme arme de catégorie A2 et donc comme *matériel de guerre*
======================================================================================================

- https://maintiendelordre.fr/grenade-lacrymogene-gm2l-sae-820/
- :ref:`glif4`


.. figure:: gm2l.jpg
   :align: center

Description
=============

:Source: https://maintiendelordre.fr/grenade-lacrymogene-gm2l-sae-820/

La grenade GM2L SAE 820 est une grenade lacrymogène instantanée produite par
l’entreprise française Alsetex.

Elle équipe les forces de l’ordre depuis 2018 et a pour objectif de remplacer
la GLI-F4 qui n’est plus produite par Alsetex.
Le 24 mai 2018, l’avis n°18-68665 paraît dans le Bulletin officiel des annonces
des marchés publics (BOAMP) et stipule que Alsetex remporte le marché des
«grenades lacrymogènes et moyens de propulsions» pour 1,8 million d’euros sur 4 ans.

Contrairement à la GLI-F4, la GM2L ne contient pas de TNT mais des éléments
pyrotechniques sans effet de souffle surement 43 g d’Hexocire, un mélange de
cire et d’héxogène (**un explosif plus puissant que la TNT**) et 15 g de CS
en poudre.  Elle peut être utilisée à la main ou avec un lanceur Cougar 56mm.

"L’explosif qui est dans la GM2L est 1,6 fois plus puissant que celui de la
:ref:GLI-F4 <glif4>`. Ce sont des armes de guerre." (https://twitter.com/LeMediaTV/status/1223293115752177665?s=20)

La GM2L est classée comme arme de catégorie A2 et donc comme *matériel de guerre*

Depuis le second trimestre de l’année 2018, la grenade GM2L a commencé à remplacer
la :ref:GLI-F4 <glif4>` et dotera l’ensemble des unités à terme.

Le 26 janvier 2020 elle remplace définitivement la :ref:`GLI-F4 <glif4>`.


Commentaires de Laurent Thines (https://twitter.com/LaurentThines) le mercredi 29 janvier 2020
================================================================================================

- https://twitter.com/LaurentThines/status/1222614131020353538?s=20

- 15 g de gaz CS (GLIF4 10g)
- 48g d’Hexocire = explosif C4 (GLIF4 15g TNT 1,6 fois moins puissant)
- explosion 165 dB = réacteur d'avion au décollage
- projection de plastique + détonateur


Commentaires de Laurent Thines (https://twitter.com/LaurentThines) le lundi 27 janvier 2020
=============================================================================================

- https://twitter.com/LaurentThines/status/1221802719259824128?s=20

GM2L: ne comporterait plus de pièces métalliques (à confirmer...) mais voici une
photo qui vous montrera les blessures par brûlure ou criblage que l’on peut
subir par la projection des parties dites « plastiques » = opérations itératives
pour extraire les corps étrangers


.. figure:: laurent_thines/cas_degats_gm2l_numero_1.png
   :align: center


