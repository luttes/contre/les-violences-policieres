

.. _soutiens_soline_2023_03_25:

============================================================
Soutiens, Cagnottes en ligne de solidarité matérielle
============================================================

- https://rebellyon.info/Solidarite-materielle-avec-Mickael-et-24728

Cagnottes en ligne de solidarité matérielle

Nombre d'entre vous ont demandé si une cagnotte de solidarité avait été
mise en place pour les blessé.e.s de Sainte-Soline et plus particulièrement
pour les 2 cas les plus graves pour lesquels leurs parents ont porté
plainte pour « tentative de meurtre et entraves au secours » ainsi que
pour « violation du secret professionnel dans le cadre de l'enquête et
détournement de l'objet de la consultation des fichiers pour un objectif
autre ».

Ce qui va induire des frais importants (santé, justice, voyages, logement...).

Elles sont en place depuis aujourd'hui sur le blog de soutien au S :

- https://lescamaradesdus.noblogs.org/cagnottes-de-soutien/

Pour Mickaël

- https://www.leetchi.com/c/mycka-gj-41?utm_source=copylink&utm_medium=social_sharing

Pour Serge :

- https://www.helloasso.com/associations/association-la-sellette/collectes/solidarite-pour-les-proches-de-serge-et-de-blesses-de-ste-soline

Attention, ne faites confiance qu'à des sources d'informations fiables
De nombreuses rumeurs plus ou moins bien intentionnées circulent sur
l'état de santé de Serge et sur le parcours politique et individuel des
deux blessés.

Ne les relayez pas. Les seules informations fiables émanent des parents
de Serge, des ses camarades et de l'avocate des deux blessés.

Elles se trouvent sur :

- le site de l'Organisation Communiste Libertaire https://oclibertaire.lautre.net/
- Le blog des camarades du S. https://lescamaradesdus.noblogs.org/cagnottes-de-soutien/
- Le site du collectif « Camarades »

  - https://camaraderevolution.org/index.php/2023/03/30/3-communiques-au-sujet-de-s-camarade-au-pronostic-vital-engage-a-la-suite-de-la-manifestation-de-sainte-soline/
