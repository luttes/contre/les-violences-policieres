
.. index::
   pair: Serge; Duteuil-Graziani

.. _serge_duteuil_graziani:

=============================
**Serge Duteuil-Graziani**
=============================


2023-04-06 Métallos en colère solidaires avec Serge et les blessés de Sainte Soline
======================================================================================

- http://cnt-ait.info/2023/04/07/metallos-sainte-soline/

Il y a un an on débutait une grève pour nos salaires (1).
Beaucoup de personnes et d’organisations étaient venues nous soutenir.

Parmi elles, Serge, aujourd’hui entre la vie et la mort, gravement
blessé par la répression féroce que l’Etat a abattu sur les manifestants
anti-bassines.

Aujourd’hui, nous tenons à exprimer notre soutien plein et entier à Serge,
ses proches, ainsi qu’à toutes et tous les blessées de Sainte-Soline.


Des métallos solidaires et en colère.



2023-04-06 **Sainte-Soline : comment le militant antibassine Serge D. a été gravement blessé**
=================================================================================================

@hugues@mastodon.zaclys.com
-------------------------------

- https://mastodon.zaclys.com/@hugues/110153331396774166

Sainte-Soline : comment le militant antibassine Serge D. a été gravement blessé

Le Monde nous délivre une magistrale enquête vidéo façon investigation
scientifique démontrant de manière éclatante combien les armes de guerre
utilisée par les FDP l'ont été sans aucun respect de la réglementation,
et en particulier le cas de Serge D, à ce jour toujours en urgence vitale,
à l'évidence victime de ces  pratiques non réglementaires

#SainteSoline #police #ViolencesPolicières


Le monde
---------

- https://www.youtube.com/watch?v=wZrsVpBy0N0


Des milliers de grenades, des cocktails molotov, des blessés par centaines :
le 25 mars 2023, à Sainte-Soline dans les Deux-Sèvres, une manifestation
contre un projet de méga bassine vire à l’affrontement.

Au moins deux manifestants, dont Serge Duteuil-Graziani, un militant de
32 ans, sont grièvement blessés. Que s’est-il passé ?

Les dizaines d’heures de vidéos capturées ce jour-là et les témoignages
recueillis par la cellule d’enquête vidéo du Monde montrent que le manifestant,
en première ligne des affrontements, a vraisemblablement été touché par
un tir non réglementaire de grenade lacrymogène, tiré depuis l’une des
zones où étaient positionnés les gendarmes.

Les images analysées prouvent aussi que Serge Duteuil-Graziani n’a pas
été touché à ce moment-là par les projectiles d’autres manifestants.

L’enquête du Monde révèle également, grâce à des images inédites de la
journaliste de Brut Camille Courcy, un autre tir non réglementaire, sur
un autre manifestant.

Un tir effectué trop à l’horizontale et réalisé avec le dispositif de
propulsion le plus puissant, qui a touché ce manifestant en pleine tête.

🚨 Retrouvez toutes nos enquêtes vidéo ici: https://www.youtube.com/playlist?list=PLFuK0VAIne9LVg-1mTFeGvisVBWdGLelc


🔴 Vous souhaitez nous contacter ? Nous envoyer des infos ou des documents confidentiels ?
Voici notre adresse : investigation@lemonde.fr
