
.. index::
   pair: Cas; Sainte-Soline (2023-03-25)
   pair: Ombeline; Sainte-Soline
   pair: Ingrid; Sainte-Soline
   pair: Sébastien; Sainte-Soline
   ! Sainte-Soline


.. _sainte_soline_2023_03_25:

==========================================================================================================================================================================================================
2023-03-25 **Ultra-Violence d'Etat à Sainte-Soline : deux manifestants dans le coma, 200 autres blessé-e-s dont 40 gravement** #NoBassaran #JeMeSoulève #GuerreSociale #ViolencesPolicières
==========================================================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Sainte-Soline
- :ref:`son_sainte_soline_2023_03_29`
- :ref:`soutiens_soline_2023_03_25`

.. figure:: images/carte_france.png
   :align: center

   https://fr.wikipedia.org/wiki/Sainte-Soline

#ViolencesPolicières #NoBassaran #JeMeSoulève #GuerreSociale  #Terrorisme #EtatFrançais


.. _replay_choc_sainte_solline_2023_04_08:

2023-04-08 **Replay choc : Manifs, la guerre est déclarée ? (sur les violences policières à Sainte-Soline)**
===============================================================================================================

- https://www.ici-grenoble.org/evenement/replay-choc-manifs-la-guerre-est-declaree-sur-les-violences-policieres-a-sainte-soline


2023-04-05 Après Sainte-Soline, des militants en état de choc, par Léa Guedj **Cette journée, ça n’était pas une manif, c’était la guerre**
=================================================================================================================================================

- https://www.blast-info.fr/articles/2023/apres-sainte-soline-des-militants-en-etat-de-choc-0HphC1NWSJ6pG-Wr2bbsyw

Les blessés qui tombent en nombre, le bruit assourdissant et continu des
grenades, la « peur de mourir »…

De nombreux manifestants témoignent de leur sidération à leur retour de
la manifestation de Sainte-Soline (Deux-Sèvres) contre les mégabassines,
samedi 25 mars 2023.

Des blessures dans les chairs, mais aussi dans les esprits.

Les organisateurs tentent d’accompagner les participants qui souffrent
de traumatismes psychologiques.

« Nous reviendrons plus forts. »

Au rassemblement de soutien aux victimes de violences policières, jeudi 30 mars 2023,
devant l’Hôtel de Ville à Paris, une jeune femme prend le micro d’une
main, une... béquille dans l’autre.
Elle explique avoir été touchée par une grenade au cours de la manifestation
contre les bassines.
Sa détermination est intacte, mais elle reste « traumatisée » :
« On est terrifiés, on a peur. Je fais des cauchemars tous les soirs.
**Cette journée, ça n’était pas une manif, c’était la guerre. »**

« Je suis traumatisée »
-------------------------

- https://video.blast-info.fr/w/cBGdd3i1qjBq6KHcbtySpK?start=1s


2023-04-05 **Sainte-Soline : images de guerre et guerre des images**
=========================================================================

- https://blogs.mediapart.fr/petrus-borel/blog/050423/sainte-soline-images-de-guerre-et-guerre-des-images
- https://www.youtube.com/watch?v=--KfC2NeW7k

De l'aveu général, nous avons assisté à Sainte-Soline à des images de guerre.

Mais aux images de guerre, puisque guerre il y a, vient aussi s’ajouter
une **guerre des images**, tant il est vrai qu’il n’y a pas de guerre sans
propagande.

Propagande que nous allons essayer de décrypter ici.

... Mais revenons à nos bassines. En tentant de disqualifier les opposants
aux mégabassines, Darmanin utilise en réalité une technique très ancienne
du capitalisme, presque aussi vieille que la capitalisme lui-même et qui
est parfaitement explicitée dans le documentaire en quatre parties de
Raoul Peck adapté d’un livre de Sven Lindqvist, Exterminez toutes ces brutes (https://www.youtube.com/watch?v=--KfC2NeW7k).

Pour justifier sa prédation, prédation des terres et prédation des corps,
le capitalisme désigne ceux qu’ils spolient comme des sauvages qu’on peut
contraindre à l’esclavage, qu’on peut massacrer en toute bonne conscience
puisqu’ils ne sont pas comme nous...




2023-04-05 Article d'Ici-grenoble Teaser : Des mégabassines en Isère ?
============================================================================

- https://www.ici-grenoble.org/article/teaser-des-megabassines-en-isere

À PROPOS DE SAINTE-SOLINE

Samedi 25 mars 2023, plus de 30 000 personnes ont manifesté contre les mégabassines
dans les Deux-Sèvres.

Face aux manifestant-e-s, 3200 gendarmes et la **volonté d'écraser le mouvement écologiste**.

Bilan : **deux manifestants dans le coma, 200 autres blessé-e-s dont 40 gravement**.

Le message est clair : pour défendre la FNSEA et l'agriculture climaticide,
**ce gouvernement est prêt à tuer**.

Pour prendre un peu de recul et comprendre la situation, `nous <https://www.ici-grenoble.org/info/pourquoi-ici-grenoble>`_ vous recommandons
plusieurs témoignages et analyses :

- Un récapitulatif des violences policières à Sainte-Soline : `Sainte-Soline : l’ampleur de la répression policière mise en cause <https://reporterre.net/Sainte-Soline-l-ampleur-de-la-repression-policiere-mise-en-cause>`_

- Le témoignage d'un médecin sur place : `Médecin à Sainte-Soline, je témoigne de la répression <https://reporterre.net/Medecin-a-Sainte-Soline-je-temoigne-de-la-repression>`_

- Les obstructions des forces de police envers les secours : `Blessés à Sainte-Soline : les secours ont-ils été freinés par les forces de police ? <https://reporterre.net/Blesses-a-Sainte-Soline-les-secours-ont-ils-ete-freines-par-les-forces-de-l-ordre>`_

- Le témoignage de deux manifestants : `Le piège de Sainte-Soline <https://lundi.am/Le-piege-de-Sainte-Soline>`_

- La stratégie de la terreur préparée par l'État : `Mégabassines : comment le gouvernement a préparé l’opinion à un mort <https://reporterre.net/Megabassines-comment-le-gouvernement-a-prepare-l-opinion-a-un-mort>`_

- La volonté de l'État de dissoudre le mouvement Les Soulèvements de la Terre : `Darmanin annonce la dissolution des Soulèvements de la Terre <https://reporterre.net/Gerald-Darmanin-annonce-la-dissolution-des-Soulevements-de-la-Terre>`_

- Le mythe du nombre de gendarmes blessés à Sainte-Soline : `Disproportion des blessures à Sainte-Soline : la fabrique indécente des chiffres officiels <https://www.flagrant-deni.fr/disproportion-des-blessures-a-sainte-soline-la-fabrique-indecente-des-chiffres-officiels/>`_

- Une analyse d'Hervé Kempf : `Après Sainte-Soline, repenser la lutte <https://reporterre.net/Apres-Sainte-Soline-repenser-la-lutte>`_

- Un communiqué des parents de S., entre la vie et la mort : `Après Sainte-Soline : deux plaintes déposées par la famille de S. pour tentative de meurtre et entrave aux secours <https://oclibertaire.lautre.net/spip.php?article3661>`_

- Un rapport instructif des Renseignements territoriaux sur les Soulèvements de la Terre : `Quand une note des renseignements fait l’éloge des Soulèvements de la Terre <https://reporterre.net/Quand-une-note-des-renseignements-fait-l-eloge-des-Soulevements-de-la-Terre>`_

- L'utilisation pour la première fois à Sainte-Soline de produits de
  marquage des manifestant-e-s : `Sainte-Soline : les autorités pistent les manifestants grâce à un produit invisible <https://reporterre.net/A-Sainte-Soline-les-gendarmes-ont-marque-les-manifestants-avec-des-produits-codes>`_


Liens
----------

- https://reporterre.net/Sainte-Soline-l-ampleur-de-la-repression-policiere-mise-en-cause
- https://reporterre.net/Medecin-a-Sainte-Soline-je-temoigne-de-la-repression
- https://reporterre.net/Blesses-a-Sainte-Soline-les-secours-ont-ils-ete-freines-par-les-forces-de-l-ordre
- https://lundi.am/Le-piege-de-Sainte-Soline
- https://reporterre.net/Megabassines-comment-le-gouvernement-a-prepare-l-opinion-a-un-mort
- https://reporterre.net/Gerald-Darmanin-annonce-la-dissolution-des-Soulevements-de-la-Terre
- https://www.flagrant-deni.fr/disproportion-des-blessures-a-sainte-soline-la-fabrique-indecente-des-chiffres-officiels/
- https://reporterre.net/Apres-Sainte-Soline-repenser-la-lutte
- https://oclibertaire.lautre.net/spip.php?article3661 (deux plaintes déposées par la famille de S. pour tentative de meurtre et entrave aux secours)
- https://reporterre.net/Quand-une-note-des-renseignements-fait-l-eloge-des-Soulevements-de-la-Terre
- https://reporterre.net/A-Sainte-Soline-les-gendarmes-ont-marque-les-manifestants-avec-des-produits-codes

.. _deux_nouvelles_plaintes_2023_04_04:

2023-04-04 Sainte-Soline : deux nouvelles plaintes déposées à la suite de blessures graves sur des manifestants
=====================================================================================================================

- https://www.lemonde.fr/planete/article/2023/04/04/sainte-soline-deux-nouvelles-plaintes-deposees-a-la-suite-de-blessures-graves-sur-des-manifestants_6168239_3244.html

Ces deux nouvelles plaintes concernent un homme né en 1995 et une femme
née en 2003, et portent à quatre le nombre de procédures concernant des
manifestants blessés le 25 mars 2023.

Cela porte à quatre le nombre de procédures concernant des manifestants
blessés le 25 mars lors de violents affrontements avec la police lors
d’une manifestation interdite à Sainte-Soline, dans les Deux-Sèvres.

Deux nouvelles plaintes ont été déposées lundi, a annoncé mardi 4 avril
le procureur de Rennes.

Ces deux nouvelles plaintes concernent un homme né en 1995, qui souffre
d’un « traumatisme du pied gauche avec fracas osseux qu’il attribue à
une grenade de désencerclement », avec une ITT fixée à un minimum de
soixante jours, et une femme née en 2003, avec un « polytraumatisme facial
très important et des blessures aux jambes » dont l’ITT est fixée
provisoirement à cent jours, a précisé lors d’une conférence de presse
Philippe Astruc.

Le parquet de Rennes est compétent pour les questions militaires, et à
ce titre a été chargé de ces enquêtes impliquant a priori des gendarmes.


2023-04-01 Sainte-Soline : la répression a coûté plus cher que la mégabassine !
===================================================================================

- https://contre-attaque.net/2023/04/01/sainte-soline-la-repression-a-coute-plus-cher-que-la-megabassine/

Combien a coûté la répression à Sainte-Soline ?
---------------------------------------------------

- https://www.humanite.fr/politique/mega-bassines/violences-d-etat-sainte-soline-la-fuite-en-avant-788213

5 millions d’euros selon `le journal l’Humanité <https://www.humanite.fr/politique/mega-bassines/violences-d-etat-sainte-soline-la-fuite-en-avant-788213>`_

Le quotidien cite l’eurodéputé Benoît Biteau, qui révèle que le dispositif
de gendarmes déployé le 25 mars s’élève à 5 millions d’euros.

Il y avait sur place 3.200 militaires, 9 hélicoptères, des drones, des blindés,
une équipe de gendarmes armés sur des quads, des fusils tirant des
marqueurs chimiques avec de l’ADN de synthèse pour identifier les manifestants
a posteriori…

L’État français a déployé le maximum de sa puissance martiale.

Dans ces millions d’euros, il faut compter les frais d’hôtel pour les
gendarmes, les déplacements, le carburant pour les hélicoptères et les
blindés…

C’est une véritable armée en déplacement qui a été mobilisée.

Il faut ajouter à ce coût celui des 5000 grenades tirées, dont le prix à
l’unité est d’environ 40 euros en moyenne, soit autour de 200.000 euros
de munitions.

.. _grenoble_2023_03_30:

2023-03-30 Grenoble **Rassemblement en soutien aux deux manifestants de Sainte-Soline dans le coma et aux 200 autres blessé-e-s**
=====================================================================================================================================

- https://www.ici-grenoble.org/evenement/rassemblement-en-soutien-aux-deux-manifestants-de-sainte-soline-dans-le-coma-et-aux-200-autres-blesse-e-s
- https://blogs.mediapart.fr/les-soulevements-de-la-terre/blog/280323/manifestants-dans-le-coma-appel-rassemblements-jeudi-30-mars-devant-les-prefecture

Jeudi 30 mars à 19h devant la Préfecture de l'Isère, un rassemblement de
soutien s'organise pour les deux manifestants de Sainte-Soline dans le coma,
les 200 autres blessé-e-s dont 40 gravement.

Samedi 25 mars, plus de 30 000 personnes ont manifesté contre les
mégabassines dans les Deux-Sèvres. Face aux manifestant-e-s, 3200 gendarmes
et la volonté d'écraser le mouvement écologiste.

**Le message est clair : pour défendre la FNSEA et l'agriculture climaticide,
ce gouvernement est prêt à tuer**.


.. _serge_d_2023_03_29:

2023-03-29 Serge D., grièvement blessé à Sainte-Soline : le récit d’une faillite des autorités
=======================================================================================================

- https://www.mediapart.fr/journal/france/290323/serge-d-grievement-blesse-sainte-soline-le-recit-d-une-faillite-des-autorites


Ce manifestant de 32 ans, touché à la tête lors de la manifestation
anti-bassine du 25 mars, se trouve toujours entre la vie et la mort.

Mediapart a pu reconstituer son itinéraire et son évacuation tardive par
les secours, sur la base des témoignages de ceux qui l’ont pris en charge
et des éléments rendus publics par les autorités.

MercrediMercredi 29 mars, les parents de Serge D., 32 ans, grièvement
blessé à Sainte-Soline quatre jours plus tôt et toujours hospitalisé
entre la vie et la mort, ont porté plainte pour « tentative de meurtre »
et « entrave aux secours », ainsi que pour la divulgation publique
d’informations sur leur fils, tirées des fichiers de police.

Le parquet de Rennes, compétent en matière militaire, est chargé de l’enquête.

Dans un communiqué, répondant à certains articles de presse alimentés
par ces fuites policières, ses parents confirment que Serge D. est
fiché S, « comme des milliers de militants dans la France d’aujourd’hui »,
et qu’il a déjà eu affaire à la justice, « comme la plupart des gens
qui se battent contre l’ordre établi ».

Ils ajoutent qu’il « a participé à de nombreux rassemblements anticapitalistes »
et que loin de « salir » leur fils, « ces actes sont tout à son honneur ».

Ils rappellent que son pronostic vital est engagé.



2023-03-29 Sainte-Soline : enquête sur les deux heures de fiasco avant la prise en charge des blessés graves
===================================================================================================================

- https://www.liberation.fr/societe/police-justice/sainte-soline-enquete-sur-les-deux-heures-de-fiasco-avant-la-prise-en-charge-des-blesses-graves-20230329_CQKWBRBJR5DXJBWCECVVDYXHBA/

Témoignages, vidéos, images aériennes, données téléphoniques...

«Libération» documente les retards et ratés des autorités pour porter
secours aux deux manifestants actuellement entre la vie et la mort.


.. _son_sainte_soline_2023_03_29:

2023-03-29 🔊 **Violences policières : la LDH appelle à ne rien lâcher ! (enregistrement sonore)**
=====================================================================================================

- https://www.ldh-france.org/violences-policieres-la-ldh-appelle-a-ne-rien-lacher/
- https://www.change.org/p/retraites-stop-%C3%A0-l-escalade-r%C3%A9pressive
- :ref:`media_2023:son_saint_soline_2023_03_29`

L’actualité des derniers jours ne fait que renforcer notre détermination
à lutter contre la violente répression en cours.

En fin de semaine dernière, des membres de plusieurs observatoires de la
LDH étaient présents à Sainte-Soline, dans les Deux-Sèvres, pour observer
le maintien de l’ordre dans le cadre des mobilisations contre les « mégabassines ».

En contradiction avec ce que prétend la préfète des Deux-Sèvres, les
équipes présentes ont observé une utilisation disproportionnée de la
force à l’encontre de l’ensemble des personnes présentes, et ce de manière
indiscriminée.

Plusieurs cas d’entraves par les forces de l’ordre à l’intervention des
secours ont été constatés.

Trois de nos avocats ont assisté à une conversation au cours de laquelle
le Samu a indiqué ne pouvoir intervenir pour secourir un blessé en état
d’urgence vitale, dès lors que le commandement avait donné l’ordre de
ne pas le faire.

Nous avons communiqué un extrait de cet enregistrement à la presse.

Nous en publions aujourd’hui l’intégralité sur `notre site Internet <https://www.ldh-france.org/violences-policieres-la-ldh-appelle-a-ne-rien-lacher/>`_, que


La situation est particulièrement grave et doit nous mobiliser toutes et tous,
c’est pourquoi nous vous invitons à `signer la pétition de la LDH <https://www.change.org/p/retraites-stop-%C3%A0-l-escalade-r%C3%A9pressive>`_ et
diffuser largement cet audio.



.. _enregistrement_2023_03_28:

2023-03-28 **Blocage des secours à Sainte-Soline : un enregistrement enfonce les autorités**
==================================================================================================

- https://www.mediapart.fr/journal/france/290323/blocage-des-secours-sainte-soline-un-enregistrement-enfonce-les-autorites
- https://www.lemonde.fr/planete/article/2023/03/28/sainte-soline-l-enregistrement-qui-prouve-que-le-samu-n-a-pas-eu-le-droit-d-intervenir_6167340_3244.html

Dans un enregistrement révélé par « Le Monde », un pompier et un opérateur
du Samu confirment que les secours ont été bloqués, alors qu’un manifestant
était en danger de mort sur le site, samedi 25 mars 2023.

Mediapart diffuse des extraits sonores qui contredisent la version des autorités.

Le document sonore – révélé par Le Monde et dont Mediapart diffuse ci-dessous
des extraits – est tiré d’une conversation entre deux hommes, un observateur
de la Ligue des droits de l’homme (LDH) et un médecin présents sur la base
arrière de la manifestation où ils assuraient le suivi des opérations à
distance, et à l’autre bout du fil, les pompiers des Deux-Sèvres, puis un
opérateur du Samu.

Dans les échanges, les services des pompiers comme du Samu affirment à
la LDH qu’ils ne peuvent pas intervenir sur place, malgré les alertes
sur l’état de santé d’un manifestant (toujours entre la vie et la mort à ce jour).

Pourtant d’autres membres de l’association de défense des droits humains
présents à ce moment-là dans la manifestation les avaient alertés sur la
situation en urgence vitale d’une personne très gravement blessée.

« On a eu un médecin sur place et on lui a expliqué la situation, c’est
qu’on n’enverra pas d’hélico ou de Smur sur place, parce qu’on a ordre
de ne pas en envoyer par les forces de l’ordre », affirme notamment l’opérateur du Samu.

Le médecin généraliste sur la base arrière de la manifestation relance
alors en expliquant être en contact avec des « observateurs sur place
[qui] disent que c’est calme depuis trente minutes et qu’il est possible
d’intervenir ».
Ce à quoi l’opérateur du Samu répond : « Je suis d’accord avec vous,
vous n’êtes pas le premier à nous le dire. Le problème, c’est que c’est
à l’appréciation des forces de l’ordre dès qu’on est sous un commandement,
qui n’est pas nous. »

Ces échanges confondants, auxquels ont assisté trois avocats
(Mes Sarah Hunet-Ciclaire, Chloé Saynac et Pierre-Antoine Cazau), contredisent
la version des autorités, mais aussi du Samu, qui ont expliqué depuis
samedi qu’aucune entrave aux secours n’avait eu lieu le jour de la manifestation.

En réaction aux éléments contenus dans l’enregistrement, la direction
du Samu des Deux-Sèvres a seulement indiqué sur Twitter, mardi soir :
« Nous n’intervenons pas en zone d’exclusion. Il n’y a pas de débat. »


.. _greenpeace_2023_03_28:

2023-03-28 **Nous condamnons l’usage de la force dans la répression des mouvements écologistes et sociaux** par greenpeace
================================================================================================================================

- https://www.greenpeace.fr/espace-presse/nous-condamnons-lusage-de-la-force-dans-la-repression-des-mouvements-ecologistes-et-sociaux/


Nous, organisations issues du mouvement environnemental et social, tenons
à apporter notre soutien aux personnes blessées ce week-end lors de la
mobilisation contre les bassines à Sainte-Soline.

**Nous avons une pensée pour toutes et tous les manifestant·es qui se sont
mobilisé·es pour défendre un accès équitable à l’eau et qui se sont trouvé·es
pris·es dans des scènes de violence**.

Nos structures respectives tiennent à rappeler qu’elles sont fermement
opposées à toute forme de violence dans leurs modes d’actions et qu’elles
condamnent cette violence dès qu’elle s’exprime. Le bilan de ce week-end
de mobilisation est insupportable.

À l’heure où nous écrivons, on parle de près de **200 blessé·es, 2 personnes
dont le pronostic vital est engagé, plusieurs mutilé·es et blessé·es graves**.

4000 grenades ont été tirées par les forces de l’ordre en 2 heures d’affrontements…

Plusieurs de nos organisations ont signé l’appel à ce rassemblement comme
aux précédents, car nous croyons que l’accaparement des ressources en eau
est une injustice et un facteur de division de la société.

D’autant que cette solution n’est pas adaptée à long terme aux problèmes
agricoles auxquels nous avons à faire face.
Elle constitue l’exemple type de la « mal adaptation » au changement
climatique.

La question de la conformité de ces mégabassines au droit pose en outre
question, les annulations d’autorisation préfectorale étant nombreuses
en deuxième instance.
Nous en appelons au respect des décisions de justice, et donc de l’Etat de droit.

Nous condamnons le recours à la violence de la part de l’État dans la
répression des mouvements écologistes et sociaux, à Sainte-Soline comme
dans les manifestations contre la réforme des retraites.

**Le droit de manifester est un pilier de notre démocratie, et la construction
d’une société écologique ne se fera pas sans la mobilisation des citoyens
et des citoyennes.**

Nos désaccords ne doivent pas se transformer en conflits et nous attendons
de la police et de la gendarmerie qu’elles encadrent dans le calme le droit
de manifester.

La répression des mouvements écologiques comme sociaux s’intensifie depuis
plusieurs années.

La Ligue des droits de l’homme en appelle à l’arrêt de cette escalade
répressive « alarmante pour notre démocratie ».
Les lanceurs d’alerte, associations, citoyens et citoyennes qui se mobilisent
de manière non violente ont toujours été un vecteur de progrès dans
l’histoire de nos sociétés.

Leur droit de se mobiliser, pacifiquement, mérite d’être respecté.

Nous appelons Emmanuel Macron et son gouvernement à se reprendre, et à
engager un débat démocratique et apaisé avec l’ensemble de la société.

Liste des signataires
----------------------------

ActionAid France, AequitaZ, Agir pour l’Environnement, Alofa Tuvalu,
Alternatiba Paris, Alternatiba, Les Amis de la Terre France, ANV Cop 21,
France Nature Environnement, Générations Futures, Greenpeace France,
La Voix Lycéenne, MAN (Mouvement pour une alternative non violente),
Notre affaire à tous, Oxfam France, Reclaim Finance, Réseau Action Climat,
Union syndicale Solidaires, Virage Energie, WECF France, Zero Waste France


.. _temoignage_annecy_2023_03_27:

2023-03-27 🇫🇷  **3 Témoignages à Annecy pour dénoncer les violences policières à Sainte-Soline**
===================================================================================================================


Lors du rassemblement organisé le jeudi 27 mars 2023 à Annecy pour dénoncer
les violences policières à Sainte-Soline, trois jeunes participant-es
ont témoigné de leurs expériences atroces vécues au cours de la manifestation
du samedi 25 mars 2023.


Vous trouverez ci-dessous le témoignage de Sébastien Rocher de la confédération
paysanne dans un premier temps.

Suivrons les témoignages de Ombeline et de Ingrid dans un second temps


Sébastien
-----------


Sébastien Rocher, de la confédération retrace ses trois jours à Sainte-Soline
(temps de lecture : 6 mn 49 s)


- https://librinfo74.fr/temoignage-de-sebastien-de-la-confederation-paysanne-present-a-sainte-soline/

.. figure:: images/sebastien.png
   :align: center

   Sébastien


Ombeline
----------

Vous trouverez ci-dessous le témoignage de Ombeline qui lit une lettre
de rupture avec l’État avec une sincérité poignante. (lecture 1 mn 31 s)


- https://librinfo74.fr/temoignage-de-ombeline-presente-a-sainte-soline/

.. figure:: images/ombeline.png
   :align: center

   Ombeline



Ingrid
---------

Vous trouverez ci-dessous le témoignage d’Ingrid dont la sincérité et la
force de ses paroles ont bouleversé et ému l’assistance. (temps de lecture 5mn 36s)


- https://librinfo74.fr/temoignage-dingrid-presente-a-sainte-soline/

.. figure:: images/ingrid.png
   :align: center

   Ingrid


Soutiens
=========

.. toctree::
   :maxdepth: 3

   soutiens/soutiens.rst


Blessées
=========

.. toctree::
   :maxdepth: 3

   blessees/blessees


   soutiens/soutiens.rst



