.. index::
   pair: Sainte-Soline ; Serge (2023-04-04)

.. _serge_2023_04_03:

===================================================================================================================================================================================================
2023-04-04 Deuxième communiqué des parents de Serge ; **Le terrorisme et la violence sont chaque jour du côté de l’État, pas de celles et ceux qui manifestent leur rejet d’un ordre destructeur**
===================================================================================================================================================================================================

- :ref:`sainte_soline_2023_03_25`

Cela fait maintenant 10 jours que Serge est dans le coma, suite à la grenade
qu’il a reçue à Sainte-Soline lors de la manifestation contre les bassines
du 25 mars 2023. Son pronostic vital est toujours engagé.

Nous et sa compagne remercions toutes les personnes (camarades, proches et anonymes)
qui ont manifesté leur soutien et leur solidarité envers lui.

Nous remercions les dizaines de milliers de camarades qui se sont exprimés
dans la rue, devant les préfectures et ailleurs, le jeudi 30 mars 2023,
contre l’ordre policier installé en France.

Nous remercions tous ceux et celles qui ont porté assistance aux blessés
pendant la manifestation, ou qui ont apporté leur témoignage concernant
la répression à Sainte-Soline, en particulier par rapport à Mickaël et à Serge.

Nous remercions enfin l’équipe médicale qui est à leurs côtés afin de
les aider à se battre pour vivre.

Ce combat pour la vie, Serge le mène avec la même force que celle qu’il
met à combattre un ordre social dont la seule finalité est de maintenir
la main de fer de la bourgeoisie sur les exploités.

Soyons solidaires de tout ce que Darmanin veut éradiquer, dissoudre, enfermer,
mutiler – du mouvement des retraites aux comités antirépression, des
futures ZAD au mouvement des blocages.

**Le terrorisme et la violence sont chaque jour du côté de l’État, pas de
celles et ceux qui manifestent leur rejet d’un ordre destructeur**.

Les parents de Serge
Le 4 avril 2023

**Merci de diffuser le plus largement possible ce communiqué.**
