
.. _soline_2023_04_03:

========================================================================================================================================================================================================================================================================================
**À Sainte-Soline, le ministère de l’intérieur a validé un plan de maintien de l’ordre mortifère, mêlant grenades explosives et repli tactique autour de la bassine, pour, selon un gendarme, « faire apparaître le côté très violent d’une partie des manifestants ».**
========================================================================================================================================================================================================================================================================================

- https://www.mediapart.fr/journal/france/030423/usage-des-armes-sainte-soline-les-six-questions-qui-genent-gerald-darmanin

:download:`pdfs/sainte_soline_Mediapart_2023_04_03.pdf`

Usage des armes à Sainte-Soline : les six questions qui gênent Gérald Darmanin

À Sainte-Soline, le ministère de l’intérieur a validé un plan de maintien
de l’ordre mortifère, mêlant grenades explosives et repli tactique autour
de la bassine, pour, selon un gendarme, « faire apparaître le côté très
violent d’une partie des manifestants ».


Autres articles

- https://www.mediapart.fr/journal/france/030423/le-conseil-d-etat-saisi-en-refere-du-probleme-de-l-identification-des-forces-de-l-ordre
