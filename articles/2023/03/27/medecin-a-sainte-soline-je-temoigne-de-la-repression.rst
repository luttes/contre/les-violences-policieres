
.. _medecin_2023_03_27:

==========================================================================
2023-03-27 **Médecin à Sainte-Soline, je témoigne de la répression**
==========================================================================


- https://reporterre.net/Medecin-a-Sainte-Soline-je-temoigne-de-la-repression


Alors que le pronostic vital d’un opposant aux mégabassines de Sainte-Soline
est toujours engagé, une médecin urgentiste lui ayant porté secours témoigne.

Elle pointe la responsabilité de la préfecture pour le retard de sa prise
en charge par les urgences.

Le week-end de mobilisation contre les mégabassines à Sainte-Soline
(Deux-Sèvres) a été marqué par de nombreuses violences policières et
des blessures très graves, avec une personne encore entre la vie et la mort.

En attendant de plus amples informations, Reporterre publie d’ores et
déjà le témoignage d’Agathe, médecin urgentiste présente à la manifestation,
qui a suivi et s’est occupée de cette personne toujours dans le coma et
au pronostic vital engagé.

Elle pointe la gravité des faits et la responsabilité de la préfecture pour
le retard de sa prise en charge par les urgences.

