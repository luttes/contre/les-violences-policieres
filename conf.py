#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import os
import platform
import sys
from datetime import datetime
from zoneinfo import ZoneInfo
import sphinx

project = "Luttes contre les violences policières "
html_title = project
author = "Humain(e)s"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}"

copyright = f"2019-{now.year}, {author} Creative Commons CC BY-NC-SA 3.0"

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Luttes contre les violences policières"

author = f"Human people"
# html_logo = "images/mahsa_jina_amini_avatar.png"
# html_favicon = "images/mahsa_jina_amini_avatar.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx_copybutton",
]
autosummary_generate = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "grenoble_infos": ("https://grenoble.frama.io/infos/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "media_2023": ("https://luttes.frama.io/media-2023/", None),
    "illiberalisme": ("https://luttes.frama.io/contre/l-illiberalisme/", None),
    "https://gassr.gitlab.io/congres_confederaux_cntf/": None,
    "https://gassr.gitlab.io/livret_accueil_cntf": None,
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://luttes.frama.io/contre/les-violences-policieres/",
    "repo_url": "https://framagit.org/luttes/contre/les-violences-policieres",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "black",
    "color_accent": "red",
    "theme_color": "black",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://grenoble.frama.io/infos/index.html",
            "internal": False,
            "title": "Infos Grenoble",
        },
        {
            "href": "https://luttes.frama.io/contre/l-illiberalisme/",
            "internal": False,
            "title": "Contre l'illiberalisme",
        },
        {
            "href": "https://grenoble.frama.io/linkertree/",
            "internal": False,
            "title": "Liens Grenobleluttes",
        },
    ],
    "heroes": {
        "index": "Luttes contre les violences policières",
    },
    "table_classes": ["plain"],
}


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"2011-{now.year}, {author} Creative Commons CC BY-NC-SA 3.0 Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |ici_grenoble| image:: /images/ici_grenoble_avatar.png
.. |solidarite| image:: /images/solidarite_avatar_32.png
"""
